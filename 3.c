#include <stdio.h>
#include <stdlib.h>

/* Prints usage of this program named progname */
void print_usage(char *progname);

/* Reads 3 same length arrays pointed to by a1, a2, a3
 * and returns their length */
int get_arrays(FILE *fp, double *a1[], double *a2[], double *a3[]);

/* Returns array which is the sum of corresponding elements of arrays
 * pointed to by a1, a2, a3 of size length*/
double *add_arrays(const double a1[], const double a2[], const double a3[],
		const size_t length);

/* Removes negative numbers from an array a of size length
 * and returns the new length */
int remove_negatives(double *a[], const size_t length);

/* Prints contents of an array a of size length to file pointed by fp */
void print_array(FILE *fp, double *a, const size_t length);

int main(int argc, char *argv[]) {
	char *in_fname, *out_fname;
	double *ar1, *ar2, *ar3;
	double *results_array;
	int arrlen;
	FILE *in, *out;

	/* Check the argument length */
	if (argc != 3) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	in_fname = argv[1];
	out_fname = argv[2];

	if (!(in = fopen(in_fname, "r"))) {
		fprintf(stderr, "ERROR: Unable to open file \"%s\" for reading.\n",
				in_fname);
	}

	ar1 = ar2 = ar3 = NULL;

	arrlen = get_arrays(in, &ar1, &ar2, &ar3);
	if (arrlen <= 0) {
		fprintf(stderr, "Error reading file on line %d\n",
				arrlen == 0 ? 1 : -arrlen);
		exit(EXIT_FAILURE);
	}

	results_array = add_arrays(ar1, ar2, ar3, arrlen);
	arrlen = remove_negatives(&results_array, arrlen);

    puts("Results:");
	print_array(stdout, results_array, arrlen);
	if (!(out = fopen(out_fname, "w"))) {
		fprintf(stderr, "Unable to open file %s for writing,"
				" the output is not written.\n", out_fname);
	} else {
		print_array(out, results_array, arrlen);
	}

	free(ar1);
	free(ar2);
	free(ar3);
	free(results_array);

	return 0;
}

void print_usage(char *progname) {
	printf("Usage: %s <input_file> <output_file>\n", progname);
}

int get_arrays(FILE *fp, double *a1[], double *a2[], double *a3[]) {
	const int BUFFER_SIZE = 512;
	char line[BUFFER_SIZE];
	int i = 0;
	double *more_a1, *more_a2, *more_a3;
	double v1, v2, v3;
	while (fgets(line, BUFFER_SIZE, fp) != NULL) {
		if (sscanf(line, "%lf %lf %lf\n", &v1, &v2, &v3) != 3) {
			puts("RETURN -I");
			return -i;
		}

		more_a1 = (double *) realloc(*a1, (i + 1) * sizeof(double));
		more_a2 = (double *) realloc(*a2, (i + 1) * sizeof(double));
		more_a3 = realloc(*a3, (i + 1) * sizeof(double));
		if (more_a1 != NULL && more_a2 != NULL && more_a3 != NULL) {
			*a1 = more_a1;
			*a2 = more_a2;
			*a3 = more_a3;
		} else { // Memory allocation failed
			fprintf(stderr, "Memory (re)allocation failed.\n");
			exit(EXIT_FAILURE);
		}

		(*a1)[i] = v1;
		(*a2)[i] = v2;
		(*a3)[i] = v3;

		++i;
	}

	return i;
}

double *add_arrays(const double a1[], const double a2[], const double a3[],
		const size_t length) {
	int i;
	double *new_arr = malloc(length * sizeof(double));

	for (i = 0; i < length; ++i) {
		new_arr[i] = a1[i] + a2[i] + a3[i];
	}
	return new_arr;
}

int remove_negatives(double *a[], const size_t length) {
	int i = 0;
	int new_length = 0;
	double *new_arr = malloc(length * sizeof(double));

	for (i = 0; i < length; ++i) {
		if ((*a)[i] >= 0) {
			new_arr[new_length++] = (*a)[i];
		}
	}
	// Shrink the array
	new_arr = realloc(new_arr, (new_length) * sizeof(double));
	free(*a);
	*a = new_arr;
	return new_length;
}

void print_array(FILE *fp, double *a, const size_t length) {
	int i;
	for (i = 0; i < length; ++i)
		fprintf(fp, "%lf\n", a[i]);
}

