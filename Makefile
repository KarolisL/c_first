all: compile run

compile:
	gcc -Wall -g 3.c -o first

run:
	./first input.txt output.txt

clean:
	rm first
